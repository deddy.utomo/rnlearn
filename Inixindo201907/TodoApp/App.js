/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { StyleSheet, ScrollView, View } from "react-native";
import Heading from "./Heading";
import Input from "./Input";
import Button from "./Button";
import TodoList from "./TodoList";
import TabBar from "./TabBar";

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions
} from "react-native/Libraries/NewAppScreen";

let todoIndex = 0


// const App = () => {
//   return (
//     <View style={styles.container}>
//       <ScrollView keyboardShouldPersistTaps="always" style={styles.content}>
//         <View />
//       </ScrollView>
//     </View>
//   );
// };

class App extends Component {
  constructor() {
    super();
    this.state = {
      inputValue: "",
      todos: [],
      type: "All"
    };

    this.submitTodo = this.submitTodo.bind(this)
    this.toggleComplete = this.toggleComplete.bind(this)
    this.deleteTodo = this.deleteTodo.bind(this)
    this.setType = this.setType.bind(this)
  }

  inputChange(inputValue) {
    console.log("input value: ", inputValue)
    this.setState({ inputValue })
  }

  submitTodo() {
    if (this.state.inputValue.match(/^\s*$/)) {
      return
    }

    const todo = {
      title: this.state.inputValue,
      todoIndex: todoIndex,
      complete: false
    }

    todoIndex++
    const todos = [...this.state.todos, todo]
    this.setState({ todos, inputValue: '' }, () => {
      console.log('State: ', this.state)

      //console.log('ini input value: ', inputValue)
    })
  }

  deleteTodo(todoIndex) {
    let { todos } = this.state
    todos = todos.filter((todo) => todo.todoIndex !== todoIndex)
    this.setState({ todos })
  }

  toggleComplete(todoIndex) {
    // Cannot read property forEach of undefined
    // let todos = this.state.todos
    let { todos } = this.state

    todos.forEach((todo) => {
      if (todo.todoIndex === todoIndex) {
        todo.complete = !todo.complete
      }
    })
    this.setState({ todos })
  }

  setType(type) {
    this.setState({ type })
  }

  render() {
    const { inputValue, todos, type } = this.state
    return (
      <View style={styles.container}>
        <ScrollView keyboardShouldPersistTaps="always" style={styles.content}>
          <Heading />
          <Input
            inputValue={inputValue}
            inputChange={(text) => this.inputChange(text)}
          />

          <TodoList
            // type make map undefined
            type={type}
            toggleComplete={this.toggleComplete}
            deleteTodo={this.deleteTodo}
            todos={todos} />
          <Button submitTodo={this.submitTodo} />

        </ScrollView>

        <TabBar type={type} setType={this.setType} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#f5f5f5"
  },
  content: {
    flex: 1,
    paddingTop: 60
  },
  scrollView: {
    backgroundColor: Colors.lighter
  }
});

export default App;
